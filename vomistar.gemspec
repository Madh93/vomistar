lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'vomistar/version'

Gem::Specification.new do |spec|
  spec.name          = 'vomistar'
  spec.version       = Vomistar::VERSION
  spec.authors       = ['Miguel Hernández']
  spec.email         = ['mhdez@protonmail.com']

  spec.summary       = 'Basic Movistar Page Object Model implementation'
  spec.description   = spec.summary
  spec.homepage      = 'https://gitlab.com/Madh93/vomistar'
  spec.license       = 'MIT'

  spec.require_paths = ['lib']
  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  # Target Ruby version
  spec.required_ruby_version = '>= 2.5.0'

  # Runtime dependencies
  spec.add_runtime_dependency 'watir', '~> 6.16.5'

  # Development dependencies
  spec.add_development_dependency 'bundler', '~> 2.0.1'
  spec.add_development_dependency 'guard-rspec', '~> 4.7.3'
  spec.add_development_dependency 'rake', '~> 12.3.2'
  spec.add_development_dependency 'rubocop', '~> 0.63.1'
end
